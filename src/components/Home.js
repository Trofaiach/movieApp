import SearchTxt from './SearchTxt';
import List from './List';
import Header from './Header';
const Home = ({
  movies,
  inputMovie,
  setInputMovie,
  setMovies,
  requestError,
}) => {
  return (
    <>
      <Header />
      <div className="content">
        <SearchTxt
          setInputMovie={setInputMovie}
          inputMovie={inputMovie}
          setMovies={setMovies}
        />
        {!!movies.length ? <List movies={movies} /> : requestError}
      </div>
    </>
  );
};
export default Home;
