import { Link } from 'react-router-dom';

const MovieList = ({ movie }) => {
  return (
    <Link to={`/${movie.imdbID}`}>
      <li className="movies-list">
        <div className="movieDiv">
          <h2 style={{ fontSize: '18px', marginTop: '10px' }}>{movie.Title}</h2>
          <div>
            <img alt="" src={movie.Poster} />
          </div>

          <div>
            <span style={{ fontWeight: '600' }}>{movie.Year}</span>
          </div>
        </div>
      </li>
    </Link>
  );
};
export default MovieList;
