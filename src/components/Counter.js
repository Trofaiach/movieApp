import { useEffect, useState } from 'react';

const Counter = () => {
  const [counter, setCounter] = useState(0);
  const [numbers, setNumbers] = useState([]);

  const arrayFunction = () => {
    const randomNumber = Math.floor(Math.random() * 8) + 3;
    setNumbers([...numbers, randomNumber]);
    const sumNumbers = numbers.reduce((a, b) => a + b, 0);
    console.log(sumNumbers, sumNumbers + randomNumber, numbers, [
      ...numbers,
      randomNumber,
    ]);
    setCounter(sumNumbers);
  };

  return (
    <div>
      <button
        style={{
          padding: '10px',
          backgroundColor: counter > 30 ? 'red' : 'green',
        }}
        onClick={() => arrayFunction()}
      >
        Klikni me Vule
      </button>

      <div>{counter}</div>
    </div>
  );
};

export default Counter;
