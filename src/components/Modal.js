import { useEffect, useState } from 'react';
import Modal from 'react-modal';
import { Link } from 'react-router-dom';

Modal.setAppElement('#root');

const ModalWindow = ({ match, setFavoriteMovies, favoriteMovies }) => {
  const [detail, setDetail] = useState('');
  const [modalIsOpen, setModalIsOpen] = useState(true);

  const getData = async value => {
    const response = await fetch(
      `http://www.omdbapi.com/?i=${value}&apikey=4a7f94b9`
    );

    const data = await response.json();

    setDetail(data);
  };
  const saveToFavorite = () => {
    setModalIsOpen(false);
    setFavoriteMovies([...favoriteMovies, detail]);
  };
  const closeModal = () => {
    setModalIsOpen(false);
  };

  useEffect(() => {
    if (match.params.id) {
      getData(match.params.id);
    }
  }, [match.params.id]);
  return (
    <Modal
      style={{
        overlay: {
          position: 'relative',
        },
        content: {
          margin: '20px 150px',
          padding: '30px 20px',
          boxShadow: '0 20px 30px 0 rgb(0, 0, 0, 0.1)',
          border: 'none',
          position: 'relative',
          overflow: 'hidden',
        },
      }}
      isOpen={modalIsOpen}
    >
      <div className="divModal">
        <h1 style={{ fontSize: '26px', fontWeight: '500', margin: '20px 0' }}>
          {detail.Title}
        </h1>
        <img
          alt=""
          style={{ placeSelf: 'center', margin: '20px 0' }}
          src={detail.Poster}
        />
        <div>{`Released: ${detail.Released}`}</div>
        <div>{`Genre: ${detail.Genre}`}</div>
        <div>{`Director: ${detail.Director}`}</div>
        <div>{` Actors: ${detail.Actors}`}</div>
        <p>{`Plot: ${detail.Plot}`}</p>
        <Link className="modalni" to="/">
          <svg
            onClick={closeModal}
            xmlns="http://www.w3.org/2000/svg"
            className="back"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M12 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2M3 12l6.414 6.414a2 2 0 001.414.586H19a2 2 0 002-2V7a2 2 0 00-2-2h-8.172a2 2 0 00-1.414.586L3 12z"
            />
          </svg>

          <svg
            onClick={saveToFavorite}
            xmlns="http://www.w3.org/2000/svg"
            className="button1"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
            />
          </svg>
        </Link>
      </div>
    </Modal>
  );
};
export default ModalWindow;
