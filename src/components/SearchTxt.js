const SearchTxt = ({ inputMovie, setInputMovie, setMovies }) => {
  const onChange = e => {
    setInputMovie(e.target.value);
    if (!e.target.value) {
      setMovies([]);
    }
  };
  return (
    <input
      value={inputMovie}
      onChange={onChange}
      type="text"
      className="search-txt"
    />
  );
};
export default SearchTxt;
