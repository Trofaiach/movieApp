import MovieList from './MovieList';
const List = ({ movies }) => {
  return (
    <div className="list">
      {movies.map((movie, i) => (
        <MovieList key={i} movie={movie} />
      ))}
    </div>
  );
};
export default List;
