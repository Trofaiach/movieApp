import FavoritesMovies from './FavoritesMovies';
import { Link } from 'react-router-dom';

const Favorite = ({
  favoriteMovies,
  setFavoriteMovies,
  setMovies,
  setInputMovie,
}) => {
  const onClick = e => {
    setFavoriteMovies(
      favoriteMovies.filter(favMov => favMov.imdbID !== e.target.id)
    );
  };
  const clearHomeWindow = () => {
    setMovies([]);
    setInputMovie('');
  };

  return (
    <>
      <header className="myFavorite">
        <h4 style={{ color: '#8e793e' }}>Favorites</h4>
        <Link className="btnHome" to="/" onClick={clearHomeWindow}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="homeIcon"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
          </svg>
          <div className="home">Home</div>
        </Link>
      </header>
      <ul className="favorite">
        {!!favoriteMovies.length &&
          favoriteMovies.map(favoriteMovies => (
            <FavoritesMovies
              key={favoriteMovies.imdbID}
              favoriteMovies={favoriteMovies}
              onClick={onClick}
            />
          ))}
      </ul>
    </>
  );
};

export default Favorite;
