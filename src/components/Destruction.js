import React from 'react';
export const object = {
  name: 'marko',
  dob: { age: 29 },
  contact: { phone: { mobile: '065 ' } },
};
const ShowDetails = ({
  data: {
    name,
    dob: { age },
    contact: {
      phone: { mobile },
    },
  },
}) => {
  //   const {
  //     name,
  //     dob: { age },
  //     contact: {
  //       phone: { mobile },
  //     },
  //   } = data;
  return (
    <div>
      Name: {name} <br /> Age: {age} <br /> Mobile:
      {mobile}
    </div>
  );
};

const ShowUser = () => {
  return <ShowDetails data={object} />;
};

export default ShowUser;
