const FavoritesMovies = ({ favoriteMovies, onClick }) => {
  return (
    <li className="hidden">
      <div style={{ margin: '10px 0' }}>
        {favoriteMovies.Title}({favoriteMovies.Year})
      </div>
      <div>
        <img
          alt=""
          style={{
            width: '100px',
            height: '150px',
            marginBottom: '30px',
            marginTop: '10px',
          }}
          src={favoriteMovies.Poster}
        />
      </div>
      <div>
        <span>
          <svg
            id={favoriteMovies.imdbID}
            onClick={onClick}
            xmlns="http://www.w3.org/2000/svg"
            className="delete"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
            />
          </svg>
        </span>
      </div>
    </li>
  );
};
export default FavoritesMovies;
