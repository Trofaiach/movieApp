import React, { useEffect, useState } from 'react';
import './styles/style.css';
import Favorite from './components/Favorite';
import Home from './components/Home';
import axios from 'axios';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ModalWindow from './components/Modal';

const App = () => {
  const [inputMovie, setInputMovie] = useState('');
  const [movies, setMovies] = useState([]);
  const [favoriteMovies, setFavoriteMovies] = useState([]);
  const [requestError, setRequestError] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const getData = async value => {
    try {
      setRequestError('');
      setIsLoading(true);
      const data = await axios.get(
        `http://www.omdbapi.com/?s=${value}&apikey=4a7f94b9`
      );
      if (data.data.Response === 'True') {
        setMovies(data.data.Search);
        setIsLoading(false);
      } else {
        throw new Error('No movie. Please type another one.');
      }
    } catch (err) {
      setRequestError(err.message);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (inputMovie) {
      getData(inputMovie);
    }
  }, [inputMovie]);

  return (
    <Router>
      <div className="App">
        {isLoading && <p>Ucitavanje...</p>}
        <Switch>
          <Route path="/" exact>
            <Home
              movies={movies}
              setMovies={setMovies}
              inputMovie={inputMovie}
              setInputMovie={setInputMovie}
              requestError={requestError}
            />
          </Route>
          <Route path="/liked">
            <Favorite
              favoriteMovies={favoriteMovies}
              setFavoriteMovies={setFavoriteMovies}
              setMovies={setMovies}
              setInputMovie={setInputMovie}
            />
          </Route>
          <Route
            path="/:id"
            component={props => (
              <ModalWindow
                {...props}
                setFavoriteMovies={setFavoriteMovies}
                favoriteMovies={favoriteMovies}
              />
            )}
          />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
